﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;


public class ConnectionClient : MonoBehaviour
{

    private static int localPort;

    private string IP;
    public int port;

    IPEndPoint remoteEndPoint;
    UdpClient client;

  
    protected void init()
    {
        IP = "192.168.137.1";
        port = 49000;

        remoteEndPoint = new IPEndPoint(IPAddress.Parse(IP), port);
        client = new UdpClient();


    }

    protected void sendString(string message)
    {
        try
        {
            byte[] data = Encoding.UTF8.GetBytes(message);
            client.Send(data, data.Length, remoteEndPoint);
        }
        catch(Exception err)
        {
            Debug.Log("Error: " + err.ToString());
        }
    }

    //send Vector2 message to server
    protected void SendVector2Message(Vector2 message)
    {
        try
        {
            //build message
            byte[] msg = new byte[sizeof(float) * 2];
            Buffer.BlockCopy(BitConverter.GetBytes(message.x), 0, msg, 0 * sizeof(float), sizeof(float));
            Buffer.BlockCopy(BitConverter.GetBytes(message.y), 0, msg, 1 * sizeof(float), sizeof(float));
            client.Send(msg, msg.Length, remoteEndPoint);
        }
        catch(Exception err)
        {
            Debug.Log("Error: " + err.ToString());
        }
    }



    // Start is called before the first frame update
    void Start()
    {
        init();
    }
    int i = 0;
    // Update is called once per frame
    void Update()
    {
        i++;
        //sendString("test string " + i);
       
        SendVector2Message(TouchInteraction.position);
    }

   
}






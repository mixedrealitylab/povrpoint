﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using System.Text;

public class NormalizedTouch
{
    public Vector2 normalizedPosition;
    public TouchPhase phase;
    public bool trackedThisFrame = false;
}

[System.Serializable]
public class TouchPinch
{
    public TouchData[] touches = new TouchData[2];
    public float Scale = 1.0f;
    public float StartDistance;
    public TouchPinch(TouchData[] touches)
    {
        this.touches[0] = touches[0];
        this.touches[1] = touches[1];

        StartDistance = (touches[0].normalizedTouchPosition - touches[1].normalizedTouchPosition).magnitude;
        //Debug.Log("startdistance is " + StartDistance);
    }
}

public enum TouchEventType
{
    TOUCH_DOWN = 0,
    TOUCH_UP = 1,
    BUTTON_DOWN = 2,
    BUTTON_UP = 3,
    MOVE = 4
};

public class NormalizedStylusEvent
{
    public Vector2 normalizedPosition;
    public TouchEventType type;
    
}


public class Communication : MonoBehaviour
{
	public delegate void remoteTouches(TouchData[] touches);

	public delegate void PinchUpdateCallback(TouchPinch pinch);
    public delegate void PinchBeginCallback(TouchPinch pinch);
	public delegate void PinchEndCallback(TouchPinch pinch);

	public static event PinchUpdateCallback OnPinchUpdate;
	public static event PinchBeginCallback OnPinchBegin;
	public static event PinchEndCallback OnPinchEnd;

    //public static event Action<NormalizedStylusEvent> OnStylusButtonUp;
    //public static event Action<NormalizedStylusEvent> OnStylusButtonDown;
    public static event Action<TouchData> OnStylusTouchUp;
    public static event Action<TouchData> OnStylusTouchDown;
    public static event Action<TouchData> OnStylusMove;

    public static event Action<TouchData[]> OnTouchDragBegin;
    public static event Action<TouchData[]> OnTouchDragUpdate;
    public static event Action<TouchData[]> OnTouchDragEnd;

    public static event Action<KeyMessage> OnKey;

    public static event remoteTouches onRemoteTouches;

 //   public int portTouch = 1337;
 //   public int portKeys = 8652;
 //   public int portStylus = 8653;
	//public int maxTouches = 5;      //most smartphones won't track more than 5 touches; keep this in sync with the smartphone app

    //public Vector2 StylusTouchInputOffset = Vector2.zero;
    //public Vector2 StylusTouchInputScale  = Vector2.one;
	

	private TouchPinch CurrentPinch = null;
	
    public static void FakeFirstButton()
    {
       
        OnKey.Invoke(new KeyMessage
        {
            keyCode = KeyCode.PageUp,
            pressed = true
        });

        OnKey.Invoke(new KeyMessage
        {
            keyCode = (KeyCode)19,
            pressed = false,
        });
    }


    public static void FakeSecondButton()
    {
        OnKey.Invoke(new KeyMessage
        {
            keyCode = KeyCode.PageDown,
            pressed = true
        });

        OnKey.Invoke(new KeyMessage
        {
            keyCode = (KeyCode)20,
            pressed = false
        });
    }

    [ContextMenu("First Pen Button")]
    public void FakeFirstButtonNonStatic()
    {
        OnKey.Invoke(new KeyMessage
        {
            keyCode = KeyCode.PageUp,
            pressed = true
        });

        OnKey.Invoke(new KeyMessage
        {
            keyCode = (KeyCode)15,
            pressed = false
        });
    }

    [ContextMenu("Second Pen Button")]
    public void FakeSecondButtonNonStatic()
    {
        OnKey.Invoke(new KeyMessage
        {
            keyCode = KeyCode.PageDown,
            pressed = true
        });

        OnKey.Invoke(new KeyMessage
        {
            keyCode = (KeyCode)20,
            pressed = false
        });
    }
    //Unity is not thread safe, yet we have to run the UDP listener in a seperate Thread. In order to still use the data in Unity,
    //we will use a thread-safe ConcurrentQueue.
    void Start()
    {
    }

    private void HandlePinch(TouchData[] touches)
    {   
        //hardcoded to two fingers for now
        if(CurrentPinch == null)
        {
            CurrentPinch  = new TouchPinch(touches);
			OnPinchBegin?.Invoke(CurrentPinch);
        }
        else
        {
            CurrentPinch.touches[0] = touches[0];
            CurrentPinch.touches[1] = touches[1];
            //check how scale changed
            var newDistance = (touches[0].normalizedTouchPosition - touches[1].normalizedTouchPosition).magnitude;

            
            var newScale =  newDistance / CurrentPinch.StartDistance;

            //print("updating scale: newdistance is " + newDistance + ", startdistance is " + CurrentPinch.StartDistance + " and scale is " + newScale);
            CurrentPinch.Scale = newScale;

            OnPinchUpdate?.Invoke(CurrentPinch);
        }
    }

    public bool printKeys = false;
    public void OnKeyMessageReceived(KeyMessage[] keys)
    {
        if(printTouches)
        {
            string msg = "key input received received: ";

            foreach(var key in keys)
            {
                msg += "code: " + key.keyCode + ", pressed: " + key.pressed + "\n";
            }

            Debug.Log(msg);
        }
        for (int i = 0; i < keys.Length; ++i)
        {
            if (keys[i].keyCode == (KeyCode)25)
            {
                var copy = new KeyMessage
                {
                    keyCode = (KeyCode)20,
                    pressed = keys[i].pressed
                };

                OnKey.Invoke(copy);
            }

            if (keys[i].keyCode == (KeyCode)23)
            {
                var copy = new KeyMessage
                {
                    keyCode = KeyCode.Pause,
                    pressed = keys[i].pressed
                };

                OnKey.Invoke(copy);
            }

            OnKey.Invoke(keys[i]);
        }

        if (printKeys)
            printRelevantKeyInfo(keys);
    }

    public bool printTouches = false;

    public void OnStylusMessageReceived(TouchData[] touches)
    {
        foreach(var t in touches)
        {
            switch (t.phase)
            {
                case TouchPhase.Began:
                    OnStylusTouchDown?.Invoke(t);
                    break;
                case TouchPhase.Ended:
                    OnStylusTouchUp?.Invoke(t);
                    break;
                //case TouchEventType.BUTTON_DOWN:
                //    OnStylusButtonDown?.Invoke(ev);
                //    break;
                //case TouchEventType.BUTTON_UP:
                //    OnStylusButtonUp?.Invoke(ev);
                //    break;
                case TouchPhase.Moved:
                    OnStylusMove?.Invoke(t);
                    break;
            }
        }
    }

    private bool Dragging = false;
    private bool CheckForDrag = false;

    public static float MinDistanceForDrag = 0.01f;
    private TouchData TouchDown;

    public void OnTouchMessageReceived(TouchData[] touches)
    {
        //sending data to all subscribers
        if (onRemoteTouches != null)
            onRemoteTouches(touches);

        if (touches.Length <= 1)
        {
            if(CurrentPinch != null)
            {
                OnPinchEnd?.Invoke(CurrentPinch);
                CurrentPinch = null;
            }

            if(!CheckForDrag && touches[0].phase == TouchPhase.Began)
            {
                CheckForDrag = true;
                TouchDown = touches[0];
            }
            else if(!Dragging && CheckForDrag && touches[0].phase == TouchPhase.Moved && (touches[0].normalizedTouchPosition - TouchDown.normalizedTouchPosition).magnitude >= MinDistanceForDrag)
            {
                Dragging = true;
                OnTouchDragBegin?.Invoke(touches);
            }
            else if(Dragging && touches[0].phase == TouchPhase.Ended)
            {
                Dragging =false;
                CheckForDrag = false;
                OnTouchDragEnd?.Invoke(touches);
            }
            else if( touches[0].phase == TouchPhase.Ended)
            {
                Dragging = false;
                CheckForDrag = false;
            }
            else if(Dragging)
            {
                OnTouchDragUpdate?.Invoke(touches);
            }
        }
        else if(touches.Length >= 2)
        {
            HandlePinch(touches);
        }

        if (printTouches)
            printRelevantTouchInfo(touches);
    }

	void printRelevantTouchInfo(TouchData[] touches)
	{
		foreach(TouchData t in touches)
			printRelevantTouchInfo(t);
	}

	void printRelevantTouchInfo(TouchData t)
	{
		Debug.Log("deltaPosition: " + t.deltaPosition);
		Debug.Log("phase: " + t.phase);
		Debug.Log("position: " + t.position);
		Debug.Log("tapCount: " + t.tapCount);
	}

    void printRelevantKeyInfo(KeyMessage[] keys)
    {
        foreach (KeyMessage k in keys)
            printRelevantKeyInfo(k);
    }
    void printRelevantKeyInfo(KeyMessage key)
    {
        Debug.Log(key);
    }
}

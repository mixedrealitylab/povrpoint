﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections.Concurrent;


abstract public class RemoteReceiver : MonoBehaviour
{
    private int basePort;       // needs to be set in inheriting class

    // private variables
    private UdpClient udpClient;

    private Thread listenThread;

    // Start is called before the first frame update
    protected void ThreadInit(int port)
    {
        this.basePort = port;

        //create new thread in background for UDP listener
        this.listenThread = new Thread(new ThreadStart(listen));
        this.listenThread.IsBackground = true;
        this.listenThread.Start();
    }


    void OnApplicationQuit()
    {
        if (this.listenThread.IsAlive)
        {
            this.listenThread.Abort();
        }
        this.udpClient.Close();
    }

    private void listen()
    {
        //create UDP listener
        this.udpClient = new UdpClient(this.basePort);
        Debug.Log("Listen for UDP on port " + this.basePort);

        while (this.udpClient != null)
        {
            try
            {
                //receive bytes and store them into queue for later processing in Update()
                IPEndPoint src = new IPEndPoint(IPAddress.Any, 0);
                //Debug.Log("try receive");
                byte[] bytes = this.udpClient.Receive(ref src);
                //Debug.Log("data received: " + bytes.Length);

                enqueueData(bytes);
            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
            }
        }
    }

    protected abstract void enqueueData(byte[] bytes);
}

﻿using System; 
using UnityEngine;
using System.IO;
using System.Text;

[Serializable]
public class TouchData
{
   
   /**
    <summary>The position delta since last change.</summary>
    */
    public Vector2 deltaPosition;

    /**
    <summary>Amount of time that has passed since the last recorded change in Touch values.</summary>
    */
    public float deltaTime;

    /**
    <summary>The unique index for the touch.</summary>
    */
    public int fingerId;

    /**
    <summary>The current amount of pressure being applied to a touch. 1.0f is considered to
             be the pressure of an average touch. If Input.touchPressureSupported returns
             false, the value of this property will always be 1.0f.</summary>
    */
    public float pressure;

    /**
    <summary>The maximum possible pressure value for a platform. If Input.touchPressureSupported
             returns false, the value of this property will always be 1.0f.</summary>
    */
    public float maximumPossiblePressure;

    /**
    <summary>Describes the phase of the touch.</summary>
    */
    public TouchPhase phase;

    /**
    <summary>The position of the touch in pixel coordinates.</summary>
    */
    public Vector2 position;    
    
    /**
    <summary>An estimated value of the radius of a touch. Add radiusVariance to get the maximum
             touch size, subtract it to get the minimum touch size.</summary>
    */
    public float radius;

    /**
    <summary>Number of taps.</summary>
    */
    public int tapCount;

    /**
    <summary>the size of the screen to get the max coordinates in x and y direction</summary>
    */
    public Vector2Int screenSize;


    /**
    <summary>the position normalized to a range of 0.0f to 1.0f depending on the screen and the orientation of the device</summary>
    */
    public Vector2 normalizedTouchPosition;

    public static byte BytesPerTouch = 60;

    public TouchData(Touch touch){
        this.deltaPosition = touch.deltaPosition;
        this.deltaTime = touch.deltaTime;
        this.fingerId = touch.fingerId;
        this.pressure = touch.pressure;
        this.maximumPossiblePressure = touch.maximumPossiblePressure;
        this.phase = touch.phase;
        this.position = touch.position;
        this.radius = touch.radius;
        this.tapCount = touch.tapCount;
        this.screenSize = new Vector2Int(Screen.width, Screen.height);
        this.normalizedTouchPosition = new Vector2(this.position.x/this.screenSize.x, this.position.y/this.screenSize.y);
    }

    public TouchData(byte[] bytes){
        // deltaPosition
        this.deltaPosition = new Vector2(
            BitConverter.ToSingle(bytes, 0),
            BitConverter.ToSingle(bytes, 4)
        );

        // deltaTime
        this.deltaTime = BitConverter.ToSingle(bytes, 8);

        // fingerId
        this.fingerId = BitConverter.ToInt32(bytes, 12);

        // maximumPossiblePressure
        this.maximumPossiblePressure = BitConverter.ToSingle(bytes, 16);

        // normalizedTouchPosition
        this.normalizedTouchPosition = new Vector2(
            BitConverter.ToSingle(bytes, 20),
            BitConverter.ToSingle(bytes, 24)
        );
        // phase
        this.phase = (TouchPhase)BitConverter.ToInt32(bytes, 28);

        // position
        this.position = new Vector2(
            BitConverter.ToSingle(bytes, 32),
            BitConverter.ToSingle(bytes, 36)
        );

        // pressure
        this.pressure = BitConverter.ToSingle(bytes, 40);

        // radius
        this.radius = BitConverter.ToSingle(bytes, 44);
        
        // screenSize
        this.screenSize = new Vector2Int(
            BitConverter.ToInt32(bytes, 48),
            BitConverter.ToInt32(bytes, 52)
        );

        // tapCount
        this.tapCount = BitConverter.ToInt32(bytes, 56);
    }

    public byte[] ToByte(){
        MemoryStream ms = new MemoryStream();
        ms = ToByte(ms);
        return ms.ToArray();
    }

    public MemoryStream ToByte(MemoryStream bytes){
        // stream in alphabetic order

        // deltaPosition
        bytes.Write(BitConverter.GetBytes(this.deltaPosition.x), 0, 4);
        bytes.Write(BitConverter.GetBytes(this.deltaPosition.y), 0, 4);

        // deltaTime
        bytes.Write(BitConverter.GetBytes(this.deltaTime), 0, 4);

        // fingerId
        bytes.Write(BitConverter.GetBytes(this.fingerId), 0, 4);

        // maximumPossiblePressure
        bytes.Write(BitConverter.GetBytes(this.maximumPossiblePressure), 0, 4);

        // normalizedTouchPosition
        bytes.Write(BitConverter.GetBytes(this.normalizedTouchPosition.x), 0, 4);
        bytes.Write(BitConverter.GetBytes(this.normalizedTouchPosition.y), 0, 4);

        // phase
        bytes.Write(BitConverter.GetBytes((int)this.phase), 0, 4);

        // position
        bytes.Write(BitConverter.GetBytes(this.position.x), 0, 4);
        bytes.Write(BitConverter.GetBytes(this.position.y), 0, 4);

        // pressure
        bytes.Write(BitConverter.GetBytes(this.pressure), 0, 4);

        // radius
        bytes.Write(BitConverter.GetBytes(this.radius), 0, 4);
        
        // screenSize
        bytes.Write(BitConverter.GetBytes(this.screenSize.x), 0, 4);
        bytes.Write(BitConverter.GetBytes(this.screenSize.y), 0, 4);

        // tapCount
        bytes.Write(BitConverter.GetBytes(this.tapCount), 0, 4);

        return bytes;
    }

    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("FingerID ").Append(this.fingerId).Append(": ");
        sb.Append("Position").Append(this.position);
        sb.Append(" | NormalizedPosition").Append(this.normalizedTouchPosition);
        sb.Append(" | TapCount = ").Append(this.tapCount);
        sb.Append(" | Phase = ").Append(this.phase);
        return sb.ToString();
    }

}

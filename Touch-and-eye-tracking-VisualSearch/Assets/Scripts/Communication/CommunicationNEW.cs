﻿//using System;
//using System.IO;
//using System.Collections;
//using System.Collections.Concurrent;
//using System.Collections.Generic;
//using System.Net;
//using System.Net.Sockets;
//using System.Threading;
//using UnityEngine;
//using System.Text;


//public class CommunicationNEW : MonoBehaviour
//{
//    public delegate void remoteTouches(TouchData[] touches);

//    public delegate void PinchUpdateCallback(TouchPinch pinch);
//    public delegate void PinchBeginCallback(TouchPinch pinch);
//    public delegate void PinchEndCallback(TouchPinch pinch);

//    public static event PinchUpdateCallback OnPinchUpdate;
//    public static event PinchBeginCallback OnPinchBegin;
//    public static event PinchEndCallback OnPinchEnd;

//    public static event Action<NormalizedStylusEvent> OnStylusButtonUp;
//    public static event Action<NormalizedStylusEvent> OnStylusButtonDown;
//    public static event Action<NormalizedStylusEvent> OnStylusTouchUp;
//    public static event Action<NormalizedStylusEvent> OnStylusTouchDown;
//    public static event Action<NormalizedStylusEvent> OnStylusMove;

//    public static event Action<TouchData> OnTouchDown;
//    public static event Action<TouchData> OnTouchUp;
//    public static event Action<TouchData[]> OnTouchDragBegin;
//    public static event Action<TouchData[]> OnTouchDragUpdate;
//    public static event Action<TouchData[]> OnTouchDragEnd;

//    public static event Action<KeyMessage> OnKey;

//    public static event remoteTouches onRemoteTouches;

//    public RemoteTouchReceiver touchReceiver; //int portTouch = 1337;
//    public RemoteKeyboardReceiver keyboardReceiver;//int portKeys = 8652;
//    public RemotePenReceiver penReceiver;//int portStylus = 8653;
//    public RemoteMouseReceiver mouseReceiver;

//    private TouchPinch CurrentPinch = null;


//    //Unity is not thread safe, yet we have to run the UDP listener in a seperate Thread. In order to still use the data in Unity,
//    //we will use a thread-safe ConcurrentQueue.
//    void Start()
//    {
//        //touchReceiver.port = portTouch;
//        touchReceiver.onRemoteTouch.AddListener(OnTouchesReceived);
//        if (keyboardReceiver)
//            keyboardReceiver.onRemoteKey.AddListener(OnKeyMessageReceived);
//        if (penReceiver)
//            penReceiver.onRemotePenEvent.AddListener(OnPenMessageReceived);
//        if (mouseReceiver)
//            mouseReceiver.onRemoteMouseEvent.AddListener(OnMouseMessageReceived);
//    }


//    private bool Dragging = false;
//    private bool CheckForDrag = false;

//    public static float MinDistanceForDrag = 0.01f;
//    private TouchData TouchDown;

//    public void OnTouchesReceived(TouchData[] touches)
//    {
//        //sending data to all subscribers
//        if (onRemoteTouches != null)
//            onRemoteTouches(touches);

//        foreach(var touch in touches)
//        {
//            if(touch.phase == UnityEngine.InputSystem.TouchPhase.Began)
//            {
//                OnTouchDown?.Invoke(touch);
//            }
//            else if(touch.phase == UnityEngine.InputSystem.TouchPhase.Ended)
//            {
//                OnTouchUp?.Invoke(touch);
//            }
//        }

//        if (touches.Length <= 1)
//        {
//            if (CurrentPinch != null)
//            {
//                OnPinchEnd?.Invoke(CurrentPinch);
//                CurrentPinch = null;
//            }

//            if (!CheckForDrag && touches[0].phase == UnityEngine.InputSystem.TouchPhase.Began)
//            {
//                CheckForDrag = true;
//                TouchDown = touches[0];
//            }
//            else if (!Dragging && CheckForDrag && touches[0].phase == UnityEngine.InputSystem.TouchPhase.Moved && (touches[0].normalizedTouchPosition - TouchDown.normalizedTouchPosition).magnitude >= MinDistanceForDrag)
//            {
//                Dragging = true;
//                OnTouchDragBegin?.Invoke(touches);
//            }
//            else if (Dragging && touches[0].phase == UnityEngine.InputSystem.TouchPhase.Ended)
//            {
//                Dragging = false;
//                CheckForDrag = false;
//                OnTouchDragEnd?.Invoke(touches);
//            }
//            else if (touches[0].phase == UnityEngine.InputSystem.TouchPhase.Ended)
//            {
//                Dragging = false;
//                CheckForDrag = false;
//            }
//            else if (Dragging)// && touches[0].phase == UnityEngine.InputSystem.TouchPhase.Moved)
//            {
//                OnTouchDragUpdate?.Invoke(touches);
//            }
//        }
//        else if (touches.Length >= 2)
//        {
//            HandlePinch(touches);
//        }

//        if (printWhatWasReceived)
//            PrintTouches(touches);
//    }



//    public class TouchPinch
//    {
//        public TouchData[] touches = new TouchData[2];
//        public float Scale = 1.0f;
//        public float StartDistance;
//        public TouchPinch(TouchData[] touches)
//        {
//            this.touches[0] = touches[0];
//            this.touches[1] = touches[1];

//            StartDistance = (touches[0].normalizedTouchPosition - touches[1].normalizedTouchPosition).magnitude;
//            //Debug.Log("startdistance is " + StartDistance);
//        }
//    }
//    private void HandlePinch(TouchData[] touches)
//    {
//        //hardcoded to two fingers for now
//        if (CurrentPinch == null)
//        {
//            CurrentPinch = new TouchPinch(touches);
//            OnPinchBegin?.Invoke(CurrentPinch);
//        }
//        else
//        {
//            CurrentPinch.touches[0] = touches[0];
//            CurrentPinch.touches[1] = touches[1];
//            //check how scale changed
//            var newDistance = (touches[0].normalizedTouchPosition - touches[1].normalizedTouchPosition).magnitude;


//            var newScale = newDistance / CurrentPinch.StartDistance;

//            //print("updating scale: newdistance is " + newDistance + ", startdistance is " + CurrentPinch.StartDistance + " and scale is " + newScale);
//            CurrentPinch.Scale = newScale;

//            OnPinchUpdate?.Invoke(CurrentPinch);
//        }
//    }


//    public bool printWhatWasReceived = false;
//    private void PrintTouches(TouchData[] touches)
//    {
//        StringBuilder sb = new StringBuilder();
//        sb.Append(touches.Length).Append(" Touches:");
//        for (int i = 0; i < touches.Length; i++)
//        {
//            sb.Append("\n\tPosition: ").Append(touches[i].position);
//            sb.Append("\n\tNormalized Position: ").Append(touches[i].normalizedTouchPosition);
//            sb.Append("\n\tPhase: ").Append(touches[i].phase);
//        }

//        Debug.Log(sb.ToString());
//    }

//    public void PrintKeys(KeyboardData[] keyboardDatas)
//    {
//        StringBuilder sb = new StringBuilder();
//        sb.Append(keyboardDatas.Length).Append(" KeyDatas:");
//        for (int i = 0; i < keyboardDatas.Length; i++)
//        {
//            sb.Append("\n\tPressed Keys(").Append(keyboardDatas[i].pressedKeys.Count).Append("): ");
//            for (int j = 0; j < keyboardDatas[i].pressedKeys.Count; j++)
//            {
//                sb.Append(keyboardDatas[i].pressedKeys[j]).Append(" | ");
//            }
//            sb.Append("\n\rReleased Keys(").Append(keyboardDatas[i].releasedKeys.Count).Append("): ");
//            for (int j = 0; j < keyboardDatas[i].releasedKeys.Count; j++)
//            {
//                sb.Append(keyboardDatas[i].releasedKeys[j]).Append(" | ");
//            }
//        }
//        Debug.Log(sb.ToString());
//    }

//    private void Update()
//    {
//        var values = Enum.GetValues(typeof(KeyCode));

//        foreach(var value in values)
//        {
//            var keyCode = (KeyCode)value;

//            if(Input.GetKeyDown(keyCode))
//            {
//                //
//            }
//            else if(Input.GetKeyUp(keyCode))
//            {
//                //
//            }
//        }
//    }

//    private Dictionary<int, bool> pressedKeys = new Dictionary<int, bool>();
//    public void OnKeyMessageReceived(KeyboardData[] keyboardDatas)
//    {
//        if (printWhatWasReceived)
//            PrintKeys(keyboardDatas);

//        foreach(var data in keyboardDatas)
//        {
//            foreach(var pressed in data.pressedKeys)
//            {
//                bool value;
//                if(!pressedKeys.TryGetValue(pressed,out value))
//                {
//                    value = pressedKeys[pressed] = false;
//                }

//                if(!value)
//                {
//                    var KeyMessage = new KeyMessage
//                    {
//                        keyCode = (KeyCode)pressed,
//                        pressed = true,
//                        //isRepeat = false
//                    };

//                    OnKey.Invoke(KeyMessage);
//                }
//                else
//                {
//                    var KeyMessage = new KeyMessage
//                    {
//                        keyCode = (KeyCode)pressed,
//                        pressed = true,
//                        //isRepeat = true
//                    };

//                    OnKey.Invoke(KeyMessage);
//                }

//                pressedKeys[pressed] = true;
//            }

//            foreach(var pressed in data.releasedKeys)
//            {
//                bool value;
//                if (!pressedKeys.TryGetValue(pressed, out value))
//                {
//                    value = pressedKeys[pressed] = false;
//                }

//                if (value)
//                {
//                    var KeyMessage = new KeyMessage
//                    {
//                        keyCode = (KeyCode)pressed,
//                        pressed = false,
//                        //isRepeat = false
//                    };

//                    OnKey.Invoke(KeyMessage);
//                }
//                else
//                {
//                    var KeyMessage = new KeyMessage
//                    {
//                        keyCode = (KeyCode)pressed,
//                        pressed = false,
//                        //isRepeat = true
//                    };

//                    OnKey.Invoke(KeyMessage);
//                }

//                pressedKeys[pressed] = false;
//            }

//        }
//    }

//    //public void OnPenMessageReceived(PenData[] penDatas)
//    //{

//    //}

//    //public void OnMouseMessageReceived(MouseData[] penDatas)
//    //{

//    //}
//}
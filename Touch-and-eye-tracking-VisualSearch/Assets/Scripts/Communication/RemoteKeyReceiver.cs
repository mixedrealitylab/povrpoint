﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class RemoteKeyEvent : UnityEvent<KeyMessage[]> { };
public class RemoteKeyReceiver : RemoteReceiver
{
    public int port = 8651;

    public RemoteKeyEvent onRemoteKey = null;

    private ConcurrentQueue<KeyMessage[]> receivedKeys;

    //Start is called before the first frame update
    void Start()
    {
        //thread-safe queue
        this.receivedKeys = new ConcurrentQueue<KeyMessage[]>();

        ThreadInit(port);
    }

    // Update is called once per frame
    void Update()
    {
        // on every update empty the queue
        while (!this.receivedKeys.IsEmpty)
        {
            try
            {
                KeyMessage[] keys;
                this.receivedKeys.TryDequeue(out keys);
                if (keys.Length > 0 && receivedKeys != null)
                {
                    onRemoteKey.Invoke(keys);
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
            }
        }
    }

    protected override void enqueueData(byte[] bytes)
    {
        //split bytes in packages and convert to Array of Keys;
        List<KeyMessage> keys = new List<KeyMessage>();

        int currentIndex = 0;
        byte[] buffer;
        while ((bytes.Length - currentIndex) > 0)
        {
            // allocate array in propper size
            buffer = new byte[KeyMessage.BytesPerKey];
            Array.Copy(bytes, currentIndex, buffer, 0, KeyMessage.BytesPerKey);
 
            // create new/received KeyMessage object
            keys.Add(new KeyMessage(buffer));

            currentIndex += KeyMessage.BytesPerKey;
        }
        this.receivedKeys.Enqueue(keys.ToArray());
    }
}

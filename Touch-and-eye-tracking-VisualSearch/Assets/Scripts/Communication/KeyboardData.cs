﻿using System;
using UnityEngine;
using System.IO;
using System.Collections.Generic;

[Serializable]
public class KeyboardData
{
    /** 
    <summary>Amount of pressed keys in list</summary>
    */
    private int pressedKeysCount;
    /** 
    <summary>Amount of released keys in list</summary>
    */
    private int releasedKeysCount;
    /** 
     <summary>A list of all keys currently pressed</summary>
     */
    public List<int> pressedKeys;
    /** 
     <summary>A list of all keys that got released this frame</summary>
     */
    public List<int> releasedKeys;

    private int last = 0;

    public KeyboardData(int[] keyCodeValues) {
        
        List<int> pressedKeys = new List<int>();
        List<int> releasedKeys = new List<int>();
        for (int i = 0; i < keyCodeValues.Length; i++)
        {
            if (Input.GetKey((KeyCode)keyCodeValues[i]))
                pressedKeys.Add(keyCodeValues[i]);
            else if (Input.GetKeyUp((KeyCode)keyCodeValues[i]))
                releasedKeys.Add(keyCodeValues[i]);
        }
        this.pressedKeys = pressedKeys;
        this.releasedKeys = releasedKeys;
        this.pressedKeysCount = pressedKeys.Count;
        this.releasedKeysCount = releasedKeys.Count;
    }
   
    public KeyboardData(byte[] bytes)
    {
        this.pressedKeysCount  = BitConverter.ToInt32(bytes, 0);
        this.releasedKeysCount = BitConverter.ToInt32(bytes, 4);

        this.pressedKeys = new List<int>();
        this.releasedKeys = new List<int>();

        int bytePos = 4;
        for (int i = 0; i < pressedKeysCount; i++)
        {
            bytePos += 4;
            this.pressedKeys.Add(BitConverter.ToInt32(bytes, bytePos));
        }
        for (int i = 0; i < releasedKeysCount; i++)
        {
            bytePos += 4;
            this.releasedKeys.Add(BitConverter.ToInt32(bytes, bytePos));
        }
    }

    public int GetCurrentSize()
    {
        return (pressedKeysCount * 4 + releasedKeysCount * 4 + 8);  // every list entry has 4byte (we only send according to count variables so actual size is not interesting) + 2*4byte for the counts
    }
    static public int CalcSize(int pressedKeysCount, int releasedKeysCount)
    {
        return (pressedKeysCount * 4 + releasedKeysCount * 4 + 8);
    }

    public byte[] ToByte()
    {
        MemoryStream ms = new MemoryStream();
        ms = ToByte(ms);
        return ms.ToArray();
    }

    public MemoryStream ToByte(MemoryStream bytes)
    {
        bytes.Write(BitConverter.GetBytes(this.pressedKeysCount), 0, 4);
        bytes.Write(BitConverter.GetBytes(this.releasedKeysCount), 0, 4);
        for (int i = 0; i < pressedKeysCount; i++)
        {
            bytes.Write(BitConverter.GetBytes(this.pressedKeys[i]), 0, 4);
        }
        for (int i = 0; i < releasedKeysCount; i++)
        {
            bytes.Write(BitConverter.GetBytes(this.releasedKeys[i]), 0, 4);
        }

        return bytes;
    }


    public override string ToString()
    {
        if (this.pressedKeys != null && this.releasedKeys != null)
        {
            System.Text.StringBuilder str = new System.Text.StringBuilder();
            foreach (int i in this.pressedKeys)
                str.Append((KeyCode)i).Append("(").Append(i).Append(") |");
            foreach (int i in this.releasedKeys)
                str.Append("| ").Append((KeyCode)i).Append("(").Append(i).Append(")");
            return str.ToString();
        }
        else return "No new KeyboardData";
    }


}

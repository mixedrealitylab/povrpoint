﻿using System;
using UnityEngine;
using System.IO;
using System.Collections.Generic;
using System.Text;

[Serializable]
public class KeyMessage
{
    /** 
    <summary>Which Key</summary>
    */
    public KeyCode keyCode;
    /** 
    <summary>Is Pressd ? -> true if pressed, false if released</summary>
    */
    public bool pressed;

    /** 
    <summary>not transmitted, only used to continue searching for other pressed keys
            -1 if no key was found or if KeyMessage was received</summary>
    */
    public int keyFoundAt = -1;


    public static byte BytesPerKey = 5;

    public KeyMessage()
    {
        keyCode = KeyCode.None;
        pressed = false;
    }
    public KeyMessage(int[] keyCodeValues, int startAT) 
    {
        for (int i = startAT; i < keyCodeValues.Length; i++)
        {
            if (Input.GetKeyDown((KeyCode)keyCodeValues[i]))
            {
                keyCode = (KeyCode)keyCodeValues[i];
                pressed = true;
                keyFoundAt = i;
            }
            else if (Input.GetKeyUp((KeyCode)keyCodeValues[i]))
            {
                keyCode = (KeyCode)keyCodeValues[i];
                pressed = false;
                keyFoundAt = i;
            }
        }
    }
   
    public KeyMessage(byte[] bytes)
    {
        this.keyCode = (KeyCode)BitConverter.ToInt32(bytes, 0);
        this.pressed = BitConverter.ToBoolean(bytes, 4);
    }

    public byte[] ToByte()
    {
        MemoryStream ms = new MemoryStream();
        ms = ToByte(ms);
        return ms.ToArray();
    }

    public MemoryStream ToByte(MemoryStream bytes)
    {
        bytes.Write(BitConverter.GetBytes((int)this.keyCode), 0, 4);
        bytes.Write(BitConverter.GetBytes(this.pressed), 0, 1);
        return bytes;
    }


    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(this.keyCode).Append("(").Append((int)this.keyCode).Append(")");
        if (pressed)
            sb.Append(" is pressed");
        else
            sb.Append(" is NOT pressed");
        return sb.ToString();
    }


}

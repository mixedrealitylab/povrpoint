﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class RemoteMultiTouchEvent : UnityEvent<TouchData[]> { };
public class RemoteTouchReceiver : RemoteReceiver
{
    public int port = 8650;

    public RemoteMultiTouchEvent onRemoteTouch = null;

    private ConcurrentQueue<TouchData[]> receivedTouches;

    // Start is called before the first frame update
    void Start()
    {
        //thread-safe queue
        this.receivedTouches = new ConcurrentQueue<TouchData[]>();

        ThreadInit(port);
    }

    private void OnEnable()
    {
    }

    // Update is called once per frame
    void Update()
    {
        // on every update empty the queue
        while (!this.receivedTouches.IsEmpty)
        {
            try
            {
                TouchData[] touches;
                this.receivedTouches.TryDequeue(out touches);
                if (touches.Length > 0 && onRemoteTouch != null)
                {
                    onRemoteTouch.Invoke(touches);
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e.ToString());
            }

        }
    }

    protected override void enqueueData(byte[] bytes)
    {
        //split bytes in packages and convert to Array of Touches;
        List<TouchData> touches = new List<TouchData>();

        int currentIndex = 0;
        while ((bytes.Length - currentIndex) >= TouchData.BytesPerTouch)
        {
            byte[] buffer = new byte[TouchData.BytesPerTouch];
            Array.Copy(bytes, currentIndex, buffer, 0, TouchData.BytesPerTouch);
            touches.Add(new TouchData(buffer));
            currentIndex += TouchData.BytesPerTouch;
        }

        this.receivedTouches.Enqueue(touches.ToArray());
    }
}

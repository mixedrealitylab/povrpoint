﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class RemoteKeyboardEvent : UnityEvent<KeyboardData[]> { };
public class RemoteKeyboardReceiver : RemoteReceiver
{
    public int port = 8651;

    public RemoteKeyboardEvent onRemoteKey = null;

    private ConcurrentQueue<KeyboardData[]> receivedKeys;

    // Start is called before the first frame update
    void Start()
    {
        //thread-safe queue
        this.receivedKeys = new ConcurrentQueue<KeyboardData[]>();

        ThreadInit(port);
    }

  

    // Update is called once per frame
    void Update()
    {
        // on every update empty the queue
        while (!this.receivedKeys.IsEmpty)
        {
            try
            {
                KeyboardData[] keys;
                this.receivedKeys.TryDequeue(out keys);
                if (keys.Length > 0 && receivedKeys != null)
                {
                    onRemoteKey.Invoke(keys);
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
            }

        }
    }

    protected override void enqueueData(byte[] bytes)
    {
        //split bytes in packages and convert to Array of Touches;
        List<KeyboardData> keys = new List<KeyboardData>();

        int currentIndex = 0;
        while ((bytes.Length - currentIndex) > 0)       // this might be a bit riskie
        {
            // get both count variables
            byte[] buffer = new byte[8];
            Array.Copy(bytes, currentIndex, buffer, 0, 8);
            int pressedKeysCount = BitConverter.ToInt32(bytes, 0);
            int releasedKeysCount = BitConverter.ToInt32(bytes, 4);

            // allocate array in propper size
            int buffSize = KeyboardData.CalcSize(pressedKeysCount, releasedKeysCount);
            buffer = new byte[buffSize];
            Array.Copy(bytes, currentIndex, buffer, 0, buffSize);
 
            // create new/received KeyboardData object
            keys.Add(new KeyboardData(buffer));

            currentIndex += buffSize;
        }

        this.receivedKeys.Enqueue(keys.ToArray());
    }
}

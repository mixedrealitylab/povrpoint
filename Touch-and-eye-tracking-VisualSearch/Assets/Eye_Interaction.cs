﻿//========= Copyright 2018, HTC Corporation. All rights reserved. ===========
using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace ViveSR.anipal.Eye
{
    public class Eye_Interaction : MonoBehaviour
    {
        //holds information about current fucus
        private FocusInfo FocusInfo;

        //game object that is currently in focus
        private GameObject currentPlane;

        //order in which gaze information should be considered
        private readonly GazeIndex[] GazePriority = new GazeIndex[] { GazeIndex.COMBINE, GazeIndex.LEFT, GazeIndex.RIGHT };

        //TODO: needed? (probably not)
        private static EyeData eyeData = new EyeData();

        //TODO: needed? (probably not)
        private bool eye_callback_registered = false;

        public Vector3 combineDirection;
        public Vector3 leftDirection;
        public Vector3 rightDirection;
        public Vector3 leftOrigin;
        public Vector3 rightOrigin;
        public Vector3 combineOrigin;

        public Transform hitObject;



        //only start if Eye module is enabled
        private void Start()
        {
            if (!SRanipal_Eye_Framework.Instance.EnableEye)
            {
                enabled = false;
                return;
            }
        }

   


 
        //update every frame
        private void Update()
        {
            //stop if SRanipal status is not working and not unsupported
            if (SRanipal_Eye_Framework.Status != SRanipal_Eye_Framework.FrameworkStatus.WORKING &&
                SRanipal_Eye_Framework.Status != SRanipal_Eye_Framework.FrameworkStatus.NOT_SUPPORT) return;

            //if callback is enabled but no callback is registered, register a callback
            if (SRanipal_Eye_Framework.Instance.EnableEyeDataCallback == true && eye_callback_registered == false)
            {
                SRanipal_Eye.WrapperRegisterEyeDataCallback(Marshal.GetFunctionPointerForDelegate((SRanipal_Eye.CallbackBasic)EyeCallback));
                eye_callback_registered = true;
            }
            //if callback is disabled but callback is registered, unregister the callback
            else if (SRanipal_Eye_Framework.Instance.EnableEyeDataCallback == false && eye_callback_registered == true)
            {
                SRanipal_Eye.WrapperUnRegisterEyeDataCallback(Marshal.GetFunctionPointerForDelegate((SRanipal_Eye.CallbackBasic)EyeCallback));
                eye_callback_registered = false;
            }


             SRanipal_Eye.GetGazeRay(GazeIndex.LEFT, out leftOrigin, out leftDirection);
             SRanipal_Eye.GetGazeRay(GazeIndex.RIGHT, out rightOrigin, out rightDirection);
             SRanipal_Eye.GetGazeRay(GazeIndex.COMBINE, out combineOrigin, out combineDirection);


            foreach (GazeIndex index in GazePriority)
            {
                Ray GazeRay;
                //               int dart_board_layer_id = LayerMask.NameToLayer("NoReflection");
                bool eye_focus;

                //get eye focus (=true if gaze hit an object) wheter callback is enabled or not
                if (eye_callback_registered)
                    eye_focus = SRanipal_Eye.Focus(index, out GazeRay, out FocusInfo, Mathf.Infinity, Mathf.Infinity, 8, eyeData);
                else
                    eye_focus = SRanipal_Eye.Focus(index, out GazeRay, out FocusInfo);
                
                //if gaze hit object
                if (eye_focus)
                {
                    //print name of object that was hit
                    //Debug.Log("focus info: "+ FocusInfo.transform.name);
                    hitObject = FocusInfo.transform;

                    //stop because current gaze index hit object
                    break;
                }
                else
                {
                    hitObject = null;
                }
            }

        }





        //unregister callback if it was previously registered
        private void Release()
        {
            if (eye_callback_registered == true)
            {
                SRanipal_Eye.WrapperUnRegisterEyeDataCallback(Marshal.GetFunctionPointerForDelegate((SRanipal_Eye.CallbackBasic)EyeCallback));
                eye_callback_registered = false;
            }
        }

        private static void EyeCallback(ref EyeData eye_data)
        {
            eyeData = eye_data;
        }



    }

}
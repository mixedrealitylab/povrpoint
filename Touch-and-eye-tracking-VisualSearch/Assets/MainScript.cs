﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;
using System.IO;

namespace ViveSR.anipal.Eye
{


    public class MainScript : MonoBehaviour
    {

        //variables that can be changed through Unity UI
        public int numberOfWindows = 9;
        public int numberOfColumns = 3;
        public float heightOfWindows = 0.5f;
        public float widthOfWindows = 0.5f;
        public float distanceToWindows;
        public float horizontalAngle = 120.0f;
        public float verticalAngle = 90.0f;
        public Material targetMaterial_preattentive;
        public Material targetMaterial;
        public Material otherMaterial;
        public Material waitMaterial;
        public Material upMaterial;
        public Material downMaterial;
        public float FOVhorizontalAngle = 30;
        public float FOVverticalAngle = 15;
        public Boolean preattentiveCondition = true;
        public int numberOfRepetitions = 10;
        public GameObject distractorWindowContainer;
        public GameObject progressWindowContainer;
        public int progressUpdateAfter = 10;
        public Boolean imagesAsTargets = false;
        public GameObject highlighter;

        //normally fixed variables
        public GameObject WindowContainer;
        public GameObject MainCamera;
        public GameObject FoVRestrictor;

        public Shader myUnlitShader;

        //private variables
        int numberOfRows;
        Vector3 cameraPosition;
        float distToRestrictor = 0.4f;
        Vector3 gazeDirection;
        Vector3 gazeOrigin;

        Boolean targetIsThere = false;
        float startTime = 0;
        int doneRepetitions = 0;

        private StreamWriter loggingWriter;
        private StreamWriter eyeWriter;
        private StreamWriter distractorWriter;

        private Boolean[] targetIsThereArray;
        private int[][] buckets;
        private int[] bucketOrderArray;
        private int targetIdx;
        private Boolean distractorTask;
        private Boolean[] distractorTaskArray;
        private Boolean progressIsShowing = false;
        private TextMesh progressText;
        private Boolean isSearaching = false;
        private int eyeWriterIndx = 0;

        //variables for image search
        public Texture[] allImageTexturesColored;
        public Texture[] allImageTexturesBnW;
        public GameObject tabletDisplay;
        private int[] listOfAllUsedImagesIndx;
        private int[] listOfTargetImagesIndx;
        private Texture[] listOfRandomDistractorImages;
        private Transform hitObj;
        private Transform targetObj;
        private float timeInImagePreview = 0.0f;
        public float previewStartTime = 0.0f;



        //variables for touch input
        private int numberOfTouches = 0;
        private Boolean multiTouchOccured = false;
        private Boolean allTouchesEnded = false;
        private Boolean someTouchOccured = false;
        private Boolean isShowingTarget = false;



        // Start is called before the first frame update
        void Start()
        {
            //create windows
            for (int i = 0; i < numberOfWindows; i++)
            {
                GameObject quad = GameObject.CreatePrimitive(PrimitiveType.Quad);
                quad.layer = 8;

                quad.transform.SetParent(WindowContainer.transform);
                quad.transform.localScale = new Vector3(widthOfWindows, heightOfWindows, heightOfWindows);
                quad.GetComponent<Renderer>().material.shader = myUnlitShader;
            }
            numberOfRows = (int)Mathf.Ceil((float)numberOfWindows / (float)numberOfColumns);
            

            //update camera position
            cameraPosition = MainCamera.transform.position;


            //initialize targetIsThereArray (ensures that target is there in 50% of cases)
            targetIsThereArray = new Boolean[numberOfRepetitions];
            for(int i = 0; i < numberOfRepetitions/2; i++)
            {
                targetIsThereArray[i] = true;
                targetIsThereArray[numberOfRepetitions - i - 1] = false;
            }
            //randomize targetIsThereArray
            shuffle(targetIsThereArray);
            

            //at the beginning the distractor task is not active
            distractorTask = false;
            //initialize order of distractor tasks
            distractorTaskArray = new Boolean[numberOfRepetitions];
            for(int i = 0; i < numberOfRepetitions/2; i++)
            {
                distractorTaskArray[i] = true;
                distractorTaskArray[numberOfRepetitions - i - 1] = false;
            }
            shuffle(distractorTaskArray);


            //initialize buckets (ensures that target appears in all 9 areas)
            buckets = new int[9][];
            int cols1 = (int)(numberOfColumns / 3);
            int cols2 = (int)(numberOfColumns - cols1) / 2;
            int cols3 = numberOfColumns - cols1 - cols2;
            int rows1 = (int)(numberOfRows /3);
            int rows2 = (int)(numberOfRows - rows1) / 2;
            int rows3 = numberOfRows - rows1 - rows2;
            //initialize each bucket
            buckets[0] = new int[rows1 * cols1];
            int j = 0;
            for(int r = 0; r < rows1; r++)
            {
                for(int c = 0; c < cols1; c++)
                {
                    buckets[0][j] = r * numberOfColumns + c;
                        j++;
                }
            }
            buckets[1] = new int[rows1 * cols2];
            j = 0;
            for (int r = 0; r < rows1; r++)
            {
                for (int c = cols1; c < cols1+cols2; c++)
                {
                    buckets[1][j] = r * numberOfColumns + c;
                    j++;
                }
            }
            buckets[2] = new int[rows1 * cols3];
            j = 0;
            for (int r = 0; r < rows1; r++)
            {
                for (int c = cols1+cols2; c < cols1 + cols2 + cols3; c++)
                {
                    buckets[2][j] = r * numberOfColumns + c;
                    j++;
                }
            }
            buckets[3] = new int[rows2 * cols1];
            j = 0;
            for (int r = rows1; r < rows1 + rows2; r++)
            {
                for (int c = 0; c < cols1; c++)
                {
                    buckets[3][j] = r * numberOfColumns + c;
                    j++;
                }
            }
            buckets[4] = new int[rows2 * cols2];
            j = 0;
            for (int r = rows1; r < rows1 + rows2; r++)
            {
                for (int c = cols1; c < cols1 + cols2; c++)
                {
                    buckets[4][j] = r * numberOfColumns + c;
                    j++;
                }
            }
            buckets[5] = new int[rows2 * cols3];
            j = 0;
            for (int r = rows1; r < rows1 + rows2; r++)
            {
                for (int c = cols1 + cols2; c < cols1 + cols2 + cols3; c++)
                {
                    buckets[5][j] = r * numberOfColumns + c;
                    j++;
                }
            }
            buckets[6] = new int[rows3 * cols1];
            j = 0;
            for (int r = rows1 + rows2; r < rows1 + rows2 + rows3; r++)
            {
                for (int c = 0; c < cols1; c++)
                {
                    buckets[6][j] = r * numberOfColumns + c;
                    j++;
                }
            }
            buckets[7] = new int[rows3 * cols2];
            j = 0;
            for (int r = rows1 + rows2; r < rows1 + rows2 + rows3; r++)
            {
                for (int c = cols1; c < cols1 + cols2; c++)
                {
                    buckets[7][j] = r * numberOfColumns + c;
                    j++;
                }
            }
            buckets[8] = new int[rows3 * cols3];
            j = 0;
            for (int r = rows1 + rows2; r < rows1 + rows2 + rows3; r++)
            {
                for (int c = cols1 + cols2; c < cols1 + cols2 + cols3; c++)
                {
                    buckets[8][j] = r * numberOfColumns + c;
                    j++;
                }
            }

            //fill bucketOrderArray
            if (!imagesAsTargets)
            {
                bucketOrderArray = new int[Mathf.CeilToInt(numberOfRepetitions / 2)];
            }
            else
            {
                bucketOrderArray = new int[Mathf.CeilToInt(numberOfRepetitions)];
            }
            for(int i = 0; i < bucketOrderArray.Length; i++)
            {
                bucketOrderArray[i] = i%9;
            }
            shuffle(bucketOrderArray);
            targetIdx = 0;
                      

            //initialize field of view restrictor
            initFoVRestrictor();

            //set near clip plane of camera
            Camera cam = MainCamera.GetComponent<Camera>();
            cam.nearClipPlane = 0.001f;


            //place windows around user
            initWindowsInWorld();


            //initialize logging data
            if (preattentiveCondition)
            {
                loggingWriter = new StreamWriter("preatt-" + FOVhorizontalAngle + "x" + FOVverticalAngle + "-" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Hour + "-" + DateTime.Now.Minute + "-" + DateTime.Now.Second + ".csv");
                //eyeWriter = new StreamWriter("Eye-preatt-" + doneRepetitions + "-" + FOVhorizontalAngle + "x" + FOVverticalAngle + "-" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Hour + "-" + DateTime.Now.Minute + "-" + DateTime.Now.Second + ".csv");
                distractorWriter = new StreamWriter("Distractor-preatt-" + FOVhorizontalAngle + "x" + FOVverticalAngle + "-" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Hour + "-" + DateTime.Now.Minute + "-" + DateTime.Now.Second + ".csv");
            }
            else
            {
                loggingWriter = new StreamWriter("NONpreatt-" + FOVhorizontalAngle + "x" + FOVverticalAngle + "-" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Hour + "-" + DateTime.Now.Minute + "-" + DateTime.Now.Second + ".csv");
                //eyeWriter = new StreamWriter("Eye-NONpreatt-" + doneRepetitions + "-" + FOVhorizontalAngle + "x" + FOVverticalAngle + "-" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Hour + "-" + DateTime.Now.Minute + "-" + DateTime.Now.Second + ".csv");
                distractorWriter = new StreamWriter("Distractor-NONpreatt-" + FOVhorizontalAngle + "x" + FOVverticalAngle + "-" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Hour + "-" + DateTime.Now.Minute + "-" + DateTime.Now.Second + ".csv");
            }
            loggingWriter.WriteLine("target was there ; right answer; tct; time in preview");
            loggingWriter.Flush();
            distractorWriter.WriteLine("target direction ; right answer; tct");
            distractorWriter.Flush();


            //initialize position of distractorWindow
            distractorWindowContainer.transform.Find("distractorWindow").localPosition = new Vector3(0, 0, distanceToWindows);
            //initialize position of progressWindow
            progressWindowContainer.transform.Find("progressWindow").localPosition = new Vector3(0, 0, distanceToWindows);
            progressText = progressWindowContainer.transform.Find("progressWindow").GetComponent<TextMesh>();
            progressText.text = "test";


            //initialize number of images that is needed
            listOfAllUsedImagesIndx = new int[numberOfWindows];
            for (int i = 0; i < listOfAllUsedImagesIndx.Length; i++)
            {
                listOfAllUsedImagesIndx[i] = i;
            }
            listOfTargetImagesIndx = new int[numberOfRepetitions];
            //initialize list of target image indices and mix it
            for(int i = 0; i < listOfTargetImagesIndx.Length; i++)
            {
                listOfTargetImagesIndx[i] = i;
            }
            shuffle(listOfTargetImagesIndx);

            listOfRandomDistractorImages = new Texture[numberOfWindows-1];


            //initialize touch input
            Communication.onRemoteTouches += Communication_onRemoteTouches;
        }

        






        // Update is called once per frame
        void Update()
        {
            //while user is searching record eye+head gaze direction
            if (isSearaching)
            {
                eyeWriterIndx++;
                gazeDirection = WindowContainer.GetComponent<Eye_Interaction>().combineDirection;
                Vector3 gazeVector = MainCamera.transform.rotation* gazeDirection;
                eyeWriter.WriteLine(gazeVector.x + ";" + gazeVector.y + ";" + gazeVector.z + ";" + eyeWriterIndx);
                eyeWriter.Flush();
            }
                        

            //update camera position
            cameraPosition = MainCamera.transform.position;


            //update FoV Restrictor position and orientation
            FoVRestrictor.transform.position = cameraPosition;
            FoVRestrictor.transform.rotation = MainCamera.transform.rotation;
              
            
            //move windows with camera           
            WindowContainer.transform.position = MainCamera.transform.position;
            distractorWindowContainer.transform.position = MainCamera.transform.position;
            progressWindowContainer.transform.position = MainCamera.transform.position;


            //handle touch input and highlighter
            if (imagesAsTargets)
            {
                handleHighlighter();
                handleTouchInput();
            }

            tabletDisplay.transform.position = MainCamera.transform.position + new Vector3(0,0,distanceToWindows);

        }




        void handleHighlighter()
        {
            if (WindowContainer.GetComponent<Eye_Interaction>().hitObject)
            {
                highlighter.SetActive(true);
                hitObj = WindowContainer.GetComponent<Eye_Interaction>().hitObject;
                //hitObj.GetComponent<Renderer>().material = targetMaterial;

                //Debug.Log("hit object: " + hitObj.name + " position: " + hitObj.position);
                highlighter.transform.position = hitObj.position + (hitObj.position-MainCamera.transform.position)*0.05f;
                highlighter.transform.localScale = new Vector3(widthOfWindows,heightOfWindows,heightOfWindows)*1.3f;
                highlighter.transform.rotation = hitObj.rotation;
            }
            else
            {
                highlighter.SetActive(false);
            }
            
        }


        //handles touch input
        void handleTouchInput()
        {
            //if multiple fingers are touching, show target image
            if (numberOfTouches > 1 && !allTouchesEnded)
            {
                tabletDisplay.SetActive(true);
                foreach (Transform child in transform)
                {
                    child.gameObject.SetActive(false);
                }
                tabletDisplay.transform.position = MainCamera.transform.position + distanceToWindows * MainCamera.transform.forward;
                if (previewStartTime == 0.0f)
                {
                    previewStartTime = Time.time;
                }
            }
            //if not, do not show target image
            else
            {
                if (!isShowingTarget)
                {
                    tabletDisplay.SetActive(false);
                    foreach (Transform child in transform)
                    {
                        child.gameObject.SetActive(true);
                    }
                }
                //if all touches ended and only one touch occured, then it was a selection
                if (allTouchesEnded)
                {
                    if (!multiTouchOccured && someTouchOccured)
                    {
                        if (isShowingTarget)
                        {
                            isShowingTarget = false;
                            tabletDisplay.SetActive(false);
                            //selection is handled
                            
                            //task is done, so searching is over and eye tracking does not need to be tracked any more
                            isSearaching = false;

                            //if this is the first time touching, the first  task is loaded
                            if (startTime == 0)
                            {
                                showTargetImage();
                                updateMaterials();
                                timeInImagePreview = 0.0f;
                                someTouchOccured = false;
                                multiTouchOccured = false;
                                return;
                            }

                            
                            //start next task
                            doneRepetitions++;
                            showTargetImage();
                            updateMaterials();
                        }
                        else
                        {
                            //record end time for the current task
                            float endTime = Time.time;

                            //check if answer was right and print it to logging file
                            if (startTime != 0)
                            {
                                if (targetObj == hitObj)
                                {
                                    loggingWriter.WriteLine(true + ";" + true + ";" + (endTime - startTime) + ";" +timeInImagePreview);
                                    loggingWriter.Flush();
                                }
                                else
                                {
                                    loggingWriter.WriteLine(true + ";" + false + ";" + (endTime - startTime) + ";" +timeInImagePreview);
                                    loggingWriter.Flush();
                                }
                            }
                            if(doneRepetitions == numberOfRepetitions-1)
                            {
                                progressWindowContainer.SetActive(true);
                                progressText.text = "DONE";
                                
                            }
                            else
                            {
                                showTargetImage();
                                isShowingTarget = true;
                            }

                            
                        }

                    }
                    if (multiTouchOccured)
                    {
                        timeInImagePreview += Time.time - previewStartTime;
                        previewStartTime = 0.0f;
                    }
                    //touches are over and were handled so remove them
                    someTouchOccured = false;
                    multiTouchOccured = false;
                }
            }
        }




        void showTargetImage()
        {
            tabletDisplay.GetComponent<Renderer>().material.mainTexture = allImageTexturesColored[listOfTargetImagesIndx[targetIdx]];
            tabletDisplay.SetActive(true);
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(false);
            }
        }



        //reacts on touches
        private void Communication_onRemoteTouches(TouchData[] touches)
        {
            someTouchOccured = true;
            numberOfTouches = touches.Length;
            //remember if a multitouch occured
            if(numberOfTouches > 1) { multiTouchOccured = true; }

            Boolean allEnded = true;
            for(int i = 0; i < touches.Length; i++)
            {
                if(touches[i].phase != TouchPhase.Ended)
                {
                    allEnded = false;
                }
            }
            allTouchesEnded = allEnded;

        }

       








        //methods for randomizing arrays
        //boolean arrays
        public void shuffle(Boolean[] arr)
        {
            for(int i = arr.Length - 1; i > 0; i--)
            {
                int rand = UnityEngine.Random.Range(0, i);
                Boolean temp = arr[i];
                arr[i] = arr[rand];
                arr[rand] = temp;
            }
        }
        //int arrays
        public void shuffle(int[] arr)
        {
            for (int i = arr.Length - 1; i > 0; i--)
            {
                int rand = UnityEngine.Random.Range(0, i);
                int temp = arr[i];
                arr[i] = arr[rand];
                arr[rand] = temp;
            }
        }




        //initialize field of view restrictor
        void initFoVRestrictor()
        {
            Transform[] restrictorPieces = FoVRestrictor.GetComponentsInChildren<Transform>();
            restrictorPieces[1].transform.localRotation = Quaternion.Euler(0, 90, 0);
            restrictorPieces[2].transform.localRotation = Quaternion.Euler(0, -90, 0);
            restrictorPieces[3].transform.localRotation = Quaternion.Euler(-90, 0, 0);
            restrictorPieces[4].transform.localRotation = Quaternion.Euler(90, 0, 0);

            float toRight = (Mathf.Tan((FOVhorizontalAngle / 2) * Mathf.Deg2Rad) * distanceToWindows); //amount that must be moved right/left to achieve horizontal angle
            restrictorPieces[1].transform.position = distanceToWindows * FoVRestrictor.transform.forward + toRight * FoVRestrictor.transform.right;
            restrictorPieces[2].transform.position = distanceToWindows * FoVRestrictor.transform.forward + toRight * -FoVRestrictor.transform.right;

            float toUp = (Mathf.Tan((FOVverticalAngle / 2) * Mathf.Deg2Rad) * distanceToWindows); //amount that must be moved up/down to achieve vertical angle
            restrictorPieces[3].transform.position = distanceToWindows * FoVRestrictor.transform.forward + toUp * FoVRestrictor.transform.up;
            restrictorPieces[4].transform.position = distanceToWindows * FoVRestrictor.transform.forward + toUp * -FoVRestrictor.transform.up;
            //Debug.Log("FOVverticalAngle/2": +FOVverticalAngle / 2 + " |  tan of angle/2: " + Mathf.Tan((FOVverticalAngle / 2) * Mathf.Deg2Rad) + " |  distToRestrictor);    
        }


               

        //place windows on sphere around user
        void initWindowsInWorld()
        {
            //offset corresponds to half of a window (so horizontalAngle is the actual angle from total left to right)
            float angleOffsetY = Mathf.Atan((heightOfWindows / 2) / distanceToWindows);
            float angleOffsetX = Mathf.Atan((widthOfWindows / 2) / distanceToWindows);
            horizontalAngle -= 2 * angleOffsetX;
            verticalAngle -= 2 * angleOffsetY;
            
            //compute horizontal angle between individual windows
            float horizontalAngleDifference = 0;
            if (numberOfColumns - 1 > 0)
            {
                horizontalAngleDifference = horizontalAngle / (numberOfColumns - 1);
            }
            //compute vertical angle between individual windows
            float verticalAngleDifference = 0;
            if (numberOfRows - 1 > 0)
            {
                verticalAngleDifference = verticalAngle / (numberOfRows - 1);
            }
            
            //vector that holds all positions
            Vector3[] positionVector = new Vector3[numberOfRows * numberOfColumns];
            int i = 0;
            for (int row = 0; row < numberOfRows; row++)
            {
                float xRot = -(verticalAngle / 2) + row * verticalAngleDifference; //vertical rotation
                for (int col = 0; col < numberOfColumns; col++)
                {
                    float yRot = -(horizontalAngle / 2) + col * horizontalAngleDifference; //horizontal rotation

                    //print("x: " + xRot + ", y: " + yRot);
                    positionVector[i] = Quaternion.Euler(xRot, yRot, 0) * new Vector3(0, 0, distanceToWindows);
                    i++;
                }
            }
            
            //assign positions to all windows
            i = 0;
            foreach (Transform child in transform)
            {
                child.transform.position = positionVector[i];

                //change rotation of windows to face camera
                child.transform.forward = child.transform.position;
                                
                i++;
            }
            
            //make distractor and progress window invisible
            distractorWindowContainer.SetActive(false);
            progressWindowContainer.SetActive(false);
        }


        
        void getListOfRandomDistractorImages(int targetIndx)
        {
            //get indx of target image in original array
            int originalTargetIndx = listOfTargetImagesIndx[targetIndx];
            Debug.Log("original target indx: " + originalTargetIndx);

            //mix listOfAllUsedImagesIndx
            shuffle(listOfAllUsedImagesIndx);
            /*String str = "";
            for (int i=0; i<listOfAllUsedImagesIndx.Length; i++)
            {
                str = str + ", " + listOfAllUsedImagesIndx[i];
            }
            Debug.Log("List of all used indx: " + str);*/
            int j = 0;
            for(int i = 0; i < listOfAllUsedImagesIndx.Length; i++)
            {
                if(listOfAllUsedImagesIndx[i] != originalTargetIndx)
                {
                    if (preattentiveCondition)
                    {
                        listOfRandomDistractorImages[j] = allImageTexturesBnW[listOfAllUsedImagesIndx[i]];
                    }
                    else
                    {
                        listOfRandomDistractorImages[j] = allImageTexturesColored[listOfAllUsedImagesIndx[i]];
                    }                    
                    j++;
                }
            }
            /*str = "";
            for (int i = 0; i < listOfRandomDistractorImages.Length; i++)
            {
                str = str + ", " + listOfRandomDistractorImages[i];
            }
            Debug.Log("List of random indx: " + str);*/
        }




        //updates the main task
        void updateMaterials()
        {
            //make distractor and progress window invisible
            distractorWindowContainer.SetActive(false);
            progressWindowContainer.SetActive(false);
            //make windows for main task visible
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(true);
            }
                        
            //decide through targetIsThereArray, if target is there or not
            targetIsThere = targetIsThereArray[doneRepetitions];
            if (imagesAsTargets) { targetIsThere = true; }

            //if target is not there, add otherMaterial to all objects
            if (!targetIsThere)
            {
                foreach (Transform child in transform)
                {
                    //set material
                    child.GetComponent<Renderer>().material = otherMaterial;
                }
            }
            //if target is there, select one object inside a bucket that gets the targetMaterial
            else
            {
                if (imagesAsTargets) { getListOfRandomDistractorImages(targetIdx); }
                //get "random" place where target should be
                Debug.Log("targetIdx: " + targetIdx);
                int randInt = UnityEngine.Random.Range(0, buckets[bucketOrderArray[targetIdx]].Length);
                randInt = buckets[bucketOrderArray[targetIdx]][randInt];
                                
                int i = 0;
                int img_i = 0;
                foreach (Transform child in transform)
                {
                    //set material
                    if (i == randInt)
                    {
                        if (imagesAsTargets)
                        {
                            targetObj = child;
                            child.GetComponent<Renderer>().material.mainTexture = allImageTexturesColored[listOfTargetImagesIndx[targetIdx]];
                            tabletDisplay.GetComponent<Renderer>().material.mainTexture = allImageTexturesColored[listOfTargetImagesIndx[targetIdx]];
                            
                        }
                        else
                        {
                            //select either preattentive or normal targetMaterial
                            if (preattentiveCondition)
                            {
                                child.GetComponent<Renderer>().material = targetMaterial_preattentive;
                            }
                            else
                            {
                                child.GetComponent<Renderer>().material = targetMaterial;
                            }
                        }
                    }
                    else
                    {
                        if (imagesAsTargets)
                        {
                            child.GetComponent<Renderer>().material.mainTexture = listOfRandomDistractorImages[img_i];
                            img_i++;
                        }
                        else
                        {
                            child.GetComponent<Renderer>().material = otherMaterial;
                        }
                    }
                    i++;
                }
                targetIdx++;
            }
            //record start time of this task
            startTime = Time.time;
            
            //make new eye-logging-file for this task
            eyeWriterIndx = 0;
            if (preattentiveCondition)
            {
                eyeWriter = new StreamWriter("Eye-preatt-" + doneRepetitions + "-" + FOVhorizontalAngle + "x" + FOVverticalAngle + "-" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Hour + "-" + DateTime.Now.Minute + "-" + DateTime.Now.Second + ".csv");
            }
            else
            {
                eyeWriter = new StreamWriter("Eye-NONpreatt-" + doneRepetitions + "-" + FOVhorizontalAngle + "x" + FOVverticalAngle + "-" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Hour + "-" + DateTime.Now.Minute + "-" + DateTime.Now.Second + ".csv");
            }
            //is true until this tasks ends by pressing a button
            isSearaching = true;
        }



        //starts the distractor tasks
        void updateDistractorTask()
        {
            //make distractor window visible
            distractorWindowContainer.SetActive(true);
            //make windows for main task invisible
            foreach(Transform child in transform)
            {
                child.gameObject.SetActive(false);
            }
            
            //check if star should point up or down and load corresponding material
            if (distractorTaskArray[doneRepetitions])
            {
                distractorWindowContainer.GetComponentInChildren<Renderer>().material = upMaterial;
            }
            else
            {
                distractorWindowContainer.GetComponentInChildren<Renderer>().material = downMaterial;
            }

            //record start time for this task
            startTime = Time.time;
        }




        //Upon pressing buttons, log time and refresh search objects
        public void rightButtonWasPressed() //-> target is there
        {
            //do nothing if images are used as targets
            if (imagesAsTargets) { return; }

            //record end time for the current task
            float endTime = Time.time;
            
            //task is done, so searching is over and eye tracking does not need to be tracked any more
            isSearaching = false;

            //if this is the first time pressing a button, the first task is loaded
            if (startTime == 0)
            {
                updateMaterials();
                return;
            }

            //if the current task was the distractor task
            if (distractorTask)
            {
                //show update on repetitions every "progressUpdateAfter"th time
                if (doneRepetitions%progressUpdateAfter == 0 && !progressIsShowing)
                {
                    //make progress window visible and others invisible
                    progressWindowContainer.SetActive(true);
                    distractorWindowContainer.SetActive(false);
                    foreach (Transform child in transform)
                    {
                        child.gameObject.SetActive(false);
                    }
                    progressIsShowing = true;

                    //if all repetitions are completed show "DONE"
                    if (doneRepetitions == numberOfRepetitions)
                    {
                        progressText.text = "DONE";
                    }
                    //otherwise show how much is done
                    else
                    {
                        progressText.text = (doneRepetitions / numberOfRepetitions)*100 + " % completed";
                    }
                    return;
                }
                progressIsShowing = false;
                
                //handle input to distractor task
                //distractor task no longer active
                distractorTask = false;
                //write in logging file if answer was right and how much time was needed
                if (distractorTaskArray[doneRepetitions])
                {
                    distractorWriter.WriteLine("up" + " ; true ; " + (endTime - startTime));
                    distractorWriter.Flush();
                }
                else
                {
                    distractorWriter.WriteLine("down" + " ; false ; " + (endTime - startTime));
                    distractorWriter.Flush();
                }


                //stop application if number of repititions is reached
                if (doneRepetitions == numberOfRepetitions) { Application.Quit(); }

                //after distractor task the main task is laoded again
                updateMaterials();
            }
            //if the current task was the main task
            else
            {
                //check if the answer was right and print it to logging file
                if (targetIsThere)
                {
                    loggingWriter.WriteLine(targetIsThere + " ; true ; " + (endTime - startTime));
                    loggingWriter.Flush();
                }
                else
                {
                    loggingWriter.WriteLine(targetIsThere + " ; false ; " + (endTime - startTime));
                    loggingWriter.Flush();
                }
                //one more repetition is completed
                doneRepetitions++;

                //now the distractor task is on again
                distractorTask = true;
                updateDistractorTask();
            }
        }



        public void leftButtonWasPressed() //-> target is NOT there
        {
            if (imagesAsTargets) { return; }

            //record end time for the current task
            float endTime = Time.time;

            //task is done, so searching is over and eye tracking does not need to be tracked any more
            isSearaching = false;

            //if this is the first time pressing a button, the first task is loaded
            if (startTime == 0)
            {
                updateMaterials();
                return;
            }

            //if the current task was the distractor task
            if (distractorTask)
            {
                //show update on repetitions every "progressUpdateAfter"th time
                if (doneRepetitions % progressUpdateAfter == 0 && !progressIsShowing)
                {
                    //make progress window visible and others invisible
                    progressWindowContainer.SetActive(true);
                    distractorWindowContainer.SetActive(false);
                    foreach (Transform child in transform)
                    {
                        child.gameObject.SetActive(false);
                    }
                    progressIsShowing = true;

                    //if all repetitions are completed show "DONE"
                    if (doneRepetitions == numberOfRepetitions)
                    {
                        progressText.text = "DONE";
                    }
                    //otherwise show how much is done
                    else
                    {
                        progressText.text = (doneRepetitions / numberOfRepetitions) * 100 + " % completed";
                    }
                    return;
                }
                progressIsShowing = false;

                //handle input to distractor task
                //distractor task no longer active
                distractorTask = false;
                //write in logging file if answer was right and how much time was needed
                if (!distractorTaskArray[doneRepetitions])
                {
                    distractorWriter.WriteLine("down" + " ; true ; " + (endTime - startTime));
                    distractorWriter.Flush();
                }
                else
                {
                    distractorWriter.WriteLine("up" + " ; false ; " + (endTime - startTime));
                    distractorWriter.Flush();
                }


                //stop application if number of repititions is reached
                if (doneRepetitions == numberOfRepetitions) { Application.Quit(); }

                //after distractor task the main task is laoded again
                updateMaterials();
            }
            //if the current task was the main task
            else
            {
                //check if the answer was right and print it to logging file
                if (!targetIsThere)
                {
                    loggingWriter.WriteLine(targetIsThere + " ; true ; " + (endTime - startTime));
                    loggingWriter.Flush();
                }
                else
                {
                    loggingWriter.WriteLine(targetIsThere + " ; false ; " + (endTime - startTime));
                    loggingWriter.Flush();
                }
                //one more repetition is completed
                doneRepetitions++;

                //now the distractor task is on again
                distractorTask = true;                
                updateDistractorTask();
            }
        }








    }



}

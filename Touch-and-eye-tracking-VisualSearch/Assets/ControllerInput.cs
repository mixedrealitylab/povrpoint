﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using System;


public class ControllerInput : MonoBehaviour
{

    // public SteamVR_Input_Sources LeftInputSource = SteamVR_Input_Sources.LeftHand;
    // public SteamVR_Input_Sources RightInputSource = SteamVR_Input_Sources.RightHand;

    public GameObject WindowContainer;

    public SteamVR_Action_Boolean grabPinch;
    public SteamVR_Input_Sources inputSource = SteamVR_Input_Sources.Any;


    // Start is called before the first frame update
    void Start()
    {
        
    }


    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Left Trigger value: " + SteamVR_Actions._default.GrabPinch.GetStateDown(SteamVR_Input_Sources.Any).ToString());

        if (SteamVR_Actions._default.GrabPinch.GetStateDown(SteamVR_Input_Sources.RightHand))
        {

            WindowContainer.GetComponent<ViveSR.anipal.Eye.MainScript>().rightButtonWasPressed();
        }
        if (SteamVR_Actions._default.GrabPinch.GetStateDown(SteamVR_Input_Sources.LeftHand))
        {
            WindowContainer.GetComponent<ViveSR.anipal.Eye.MainScript>().leftButtonWasPressed();
        }

    }


 

}
